use crate::*;
use fastnoise2_sys as raw;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct MetaVariable(NodeType, i32);

impl MetaVariable {
	pub fn new(id: NodeType, variable_index: i32) -> Self {
		assert!(variable_index < id.variable_count());
		Self(id, variable_index)
	}

	pub fn item(self) -> NodeType {
		self.0
	}

	pub fn index(self) -> i32 {
		self.1
	}

	pub fn name(self) -> String {
		unsafe {
			let name = raw::fnGetMetadataVariableName(self.0.index(), self.1);
			std::ffi::CStr::from_ptr(name)
		}
		.to_string_lossy()
		.into_owned()
	}

	pub fn ty(self) -> VariableType {
		VariableType::try_from(unsafe { raw::fnGetMetadataVariableType(self.0.index(), self.1) })
			.unwrap()
	}

	pub fn dimension(self) -> Option<VariableDimension> {
		VariableDimension::try_from(unsafe {
			raw::fnGetMetadataVariableDimensionIdx(self.0.index(), self.1)
		})
		.ok()
	}

	pub fn iter_enum(self) -> impl Iterator<Item = MetaEnum> {
		(0..MetaEnum::count(self)).map(move |enum_i| MetaEnum::new(self, enum_i))
	}
}

#[derive(Debug)]
pub enum VariableType {
	Float,
	Int,
	Enum,
}

impl TryFrom<i32> for VariableType {
	type Error = ();

	fn try_from(value: i32) -> Result<Self, Self::Error> {
		match value {
			0 => Ok(Self::Float),
			1 => Ok(Self::Int),
			2 => Ok(Self::Enum),
			_ => Err(()),
		}
	}
}

#[derive(Debug)]
pub enum VariableDimension {
	X,
	Y,
	Z,
	W,
}

impl TryFrom<i32> for VariableDimension {
	type Error = ();

	fn try_from(value: i32) -> Result<Self, Self::Error> {
		match value {
			0 => Ok(Self::X),
			1 => Ok(Self::Y),
			2 => Ok(Self::Z),
			3 => Ok(Self::W),
			_ => Err(()),
		}
	}
}
