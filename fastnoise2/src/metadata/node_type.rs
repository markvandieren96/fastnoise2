use crate::*;
use fastnoise2_sys as raw;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum NodeType {
	Constant,
	White,
	Checkerboard,
	SineWave,
	PositionOutput,
	DistanceToPoint,
	Value,
	Perlin,
	Simplex,
	OpenSimplex2,
	CellularValue,
	CellularDistance,
	CellularLookup,
	FractalFBm,
	FractalPingPong,
	FractalRidged,
	DomainWarpGradient,
	DomainWarpFractalProgressive,
	DomainWarpFractalIndependant,
	DomainScale,
	DomainOffset,
	DomainRotate,
	SeedOffset,
	Remap,
	ConvertRGBA8,
	Add,
	Subtract,
	Multiply,
	Divide,
	Min,
	Max,
	MinSmooth,
	MaxSmooth,
	Fade,
	Terrace,
	PowFloat,
	PowInt,
	DomainAxisScale,
	AddDimension,
	RemoveDimension,
	GeneratorCache,
}

impl TryFrom<usize> for NodeType {
	type Error = ();

	fn try_from(value: usize) -> Result<Self, Self::Error> {
		match value {
			0 => Ok(Self::Constant),
			1 => Ok(Self::White),
			2 => Ok(Self::Checkerboard),
			3 => Ok(Self::SineWave),
			4 => Ok(Self::PositionOutput),
			5 => Ok(Self::DistanceToPoint),
			6 => Ok(Self::Value),
			7 => Ok(Self::Perlin),
			8 => Ok(Self::Simplex),
			9 => Ok(Self::OpenSimplex2),
			10 => Ok(Self::CellularValue),
			11 => Ok(Self::CellularDistance),
			12 => Ok(Self::CellularLookup),
			13 => Ok(Self::FractalFBm),
			14 => Ok(Self::FractalPingPong),
			15 => Ok(Self::FractalRidged),
			16 => Ok(Self::DomainWarpGradient),
			17 => Ok(Self::DomainWarpFractalProgressive),
			18 => Ok(Self::DomainWarpFractalIndependant),
			19 => Ok(Self::DomainScale),
			20 => Ok(Self::DomainOffset),
			21 => Ok(Self::DomainRotate),
			22 => Ok(Self::SeedOffset),
			23 => Ok(Self::Remap),
			24 => Ok(Self::ConvertRGBA8),
			25 => Ok(Self::Add),
			26 => Ok(Self::Subtract),
			27 => Ok(Self::Multiply),
			28 => Ok(Self::Divide),
			29 => Ok(Self::Min),
			30 => Ok(Self::Max),
			31 => Ok(Self::MinSmooth),
			32 => Ok(Self::MaxSmooth),
			33 => Ok(Self::Fade),
			34 => Ok(Self::Terrace),
			35 => Ok(Self::PowFloat),
			36 => Ok(Self::PowInt),
			37 => Ok(Self::DomainAxisScale),
			38 => Ok(Self::AddDimension),
			39 => Ok(Self::RemoveDimension),
			40 => Ok(Self::GeneratorCache),
			_ => Err(()),
		}
	}
}

impl NodeType {
	pub fn count() -> i32 {
		unsafe { raw::fnGetMetadataCount() }
	}

	pub fn iter() -> impl Iterator<Item = NodeType> {
		(0..(NodeType::count() as usize)).map(|i| NodeType::try_from(i).unwrap())
	}

	pub fn index(self) -> i32 {
		self as i32
	}

	pub fn variable_count(self) -> i32 {
		unsafe { raw::fnGetMetadataVariableCount(self as i32) }
	}

	pub fn name(self) -> String {
		unsafe {
			let name = raw::fnGetMetadataName(self as i32);
			std::ffi::CStr::from_ptr(name)
		}
		.to_string_lossy()
		.into_owned()
	}

	pub fn iter_variables(self) -> impl Iterator<Item = MetaVariable> {
		(0..self.variable_count()).map(move |var_i| MetaVariable::new(self, var_i))
	}

	pub fn node_lookup_count(self) -> i32 {
		unsafe { raw::fnGetMetadataNodeLookupCount(self as i32) }
	}

	pub fn node_lookup_name(self, node_lookup_index: i32) -> String {
		//TODO assert node_lookup_index is valid

		unsafe {
			let name = raw::fnGetMetadataNodeLookupName(self as i32, node_lookup_index);
			std::ffi::CStr::from_ptr(name)
		}
		.to_string_lossy()
		.into_owned()
	}

	pub fn node_lookup_dimension_idx(self, node_lookup_index: i32) -> i32 {
		unsafe { raw::fnGetMetadataNodeLookupDimensionIdx(self as i32, node_lookup_index) }
	}

	pub fn hybrid_count(self) -> i32 {
		unsafe { raw::fnGetMetadataHybridCount(self as i32) }
	}

	pub fn hybrid_name(self, hybrid_index: i32) -> String {
		//TODO assert hybrid_Index is valid
		unsafe {
			let name = raw::fnGetMetadataHybridName(self as i32, hybrid_index);
			std::ffi::CStr::from_ptr(name)
		}
		.to_string_lossy()
		.into_owned()
	}

	pub fn hybrid_dimension_idx(self, hybrid_index: i32) -> i32 {
		//TODO assert hybrid_Index is valid
		unsafe { raw::fnGetMetadataHybridDimensionIdx(self as i32, hybrid_index) }
	}

	pub fn inputs(self) -> impl Iterator<Item = NodeInput> {
		(0..self.variable_count())
			.map(move |var_i| MetaVariable::new(self, var_i))
			.map(|var| match var.ty() {
				VariableType::Float => NodeInput::Float {
					name: var.name(),
					index: VariableIndex(var.index()),
				},
				VariableType::Int => NodeInput::Int {
					name: var.name(),
					index: VariableIndex(var.index()),
				},
				VariableType::Enum => {
					let variants: Vec<_> = var.iter_enum().map(|e| e.name()).collect();
					NodeInput::Enum {
						name: var.name(),
						index: VariableIndex(var.index()),

						variants,
					}
				}
			})
			.chain((0..self.node_lookup_count()).map(move |i| NodeInput::Node {
				name: self.node_lookup_name(i),
				index: NodeIndex(i),
			}))
			.chain((0..self.hybrid_count()).map(move |i| NodeInput::Hybrid {
				name: self.hybrid_name(i),
				index: HybridIndex(i),
			}))
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct VariableIndex(pub i32);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct NodeIndex(pub i32);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct HybridIndex(pub i32);

#[derive(Debug)]
pub enum NodeInput {
	Float {
		name: String,
		index: VariableIndex,
	},
	Int {
		name: String,
		index: VariableIndex,
	},
	Enum {
		name: String,
		index: VariableIndex,
		variants: Vec<String>,
	},
	Node {
		name: String,
		index: NodeIndex,
	},
	Hybrid {
		name: String,
		index: HybridIndex,
	},
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_metadata() {
		for node_type in NodeType::iter() {
			println!("[{}]", node_type.name());

			for node_i in 0..node_type.node_lookup_count() {
				println!(
					"\tchild_node: {}, dim: {}",
					node_type.node_lookup_name(node_i),
					node_type.node_lookup_dimension_idx(node_i)
				);
			}

			for hybrid_i in 0..node_type.hybrid_count() {
				println!(
					"\thybrid: {}, dim: {}",
					node_type.hybrid_name(hybrid_i),
					node_type.hybrid_dimension_idx(hybrid_i)
				);
			}
		}
	}

	#[test]
	fn print_inputs() {
		for node_type in NodeType::iter() {
			println!("[{}]", node_type.name());

			for var in node_type.inputs() {
				println!("\t{:?}", var);
			}
		}
	}
}
