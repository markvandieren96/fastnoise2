use crate::*;
use fastnoise2_sys as raw;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct MetaEnum(MetaVariable, i32);

impl MetaEnum {
	pub fn count(variable_index: MetaVariable) -> i32 {
		unsafe {
			raw::fnGetMetadataEnumCount(variable_index.item().index(), variable_index.index())
		}
	}

	pub fn new(variable_index: MetaVariable, enum_index: i32) -> Self {
		assert!(enum_index < Self::count(variable_index));
		Self(variable_index, enum_index)
	}

	pub fn variable(self) -> MetaVariable {
		self.0
	}

	pub fn index(self) -> i32 {
		self.1
	}

	pub fn name(self) -> String {
		unsafe {
			let name = raw::fnGetMetadataEnumName(self.0.item().index(), self.0.index(), self.1);
			std::ffi::CStr::from_ptr(name)
		}
		.to_string_lossy()
		.into_owned()
	}
}
