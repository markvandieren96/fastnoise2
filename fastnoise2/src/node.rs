use super::*;
use fastnoise2_sys as raw;
use fastnoise2_sys::libc;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Seed(pub i32);

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct Frequency(pub f32);

pub struct Node {
	ptr: *mut libc::c_void,
	children: Vec<Node>,
}

impl Node {
	pub fn new(ty: NodeType) -> Node {
		Self::new_x(ty, SimdLevel::Null)
	}

	pub fn new_x(ty: NodeType, simd_level: SimdLevel) -> Node {
		let generator = unsafe { raw::fnNewFromMetadata(ty as i32, simd_level as u32) };
		assert!(!generator.is_null());
		Self {
			ptr: generator,
			children: Vec::new(),
		}
	}

	pub fn new_from_tree(encoded_node_tree_string: &str) -> Result<Self, ConstructionError> {
		Self::new_from_tree_x(encoded_node_tree_string, SimdLevel::Null)
	}

	pub fn new_from_tree_x(
		encoded_node_tree_string: &str,
		simd_level: SimdLevel,
	) -> Result<Self, ConstructionError> {
		let s = std::ffi::CString::new(encoded_node_tree_string)
			.map_err(ConstructionError::EncodedNodeTreeStringHasANulByte)?;
		let generator = unsafe { raw::fnNewFromEncodedNodeTree(s.as_ptr(), simd_level as u32) };
		if generator.is_null() {
			return Err(ConstructionError::NotAValidNodeTree);
		}
		Ok(Self {
			ptr: generator,
			children: Vec::new(),
		})
	}

	pub fn simd_level(&self) -> SimdLevel {
		let level = unsafe { raw::fnGetSIMDLevel(self.ptr) };
		level.into()
	}

	#[allow(dead_code)]
	pub(crate) fn metadata_id(&self) -> i32 {
		unsafe { raw::fnGetMetadataID(self.ptr) }
	}

	pub fn gen_uniform_grid_2d(
		&self,
		noise_out: &mut [f32],
		x_start: i32,
		y_start: i32,
		x_size: i32,
		y_size: i32,
		frequency: Frequency,
		seed: Seed,
		output_min_max: Option<&mut [f32; 2]>,
	) {
		assert!(noise_out.len() == (x_size * y_size).try_into().unwrap());
		unsafe {
			raw::fnGenUniformGrid2D(
				self.ptr,
				noise_out.as_mut_ptr(),
				x_start,
				y_start,
				x_size,
				y_size,
				frequency.0,
				seed.0,
				output_min_max
					.map(|s| s.as_mut_ptr())
					.unwrap_or(std::ptr::null_mut()),
			);
		}
	}

	pub fn gen_uniform_grid_3d(
		&self,
		noise_out: &mut [f32],
		x_start: i32,
		y_start: i32,
		z_start: i32,
		x_size: i32,
		y_size: i32,
		z_size: i32,
		frequency: Frequency,
		seed: Seed,
		output_min_max: Option<&mut [f32; 2]>,
	) {
		assert!(noise_out.len() == (x_size * y_size * z_size).try_into().unwrap());
		unsafe {
			raw::fnGenUniformGrid3D(
				self.ptr,
				noise_out.as_mut_ptr(),
				x_start,
				y_start,
				z_start,
				x_size,
				y_size,
				z_size,
				frequency.0,
				seed.0,
				output_min_max
					.map(|s| s.as_mut_ptr())
					.unwrap_or(std::ptr::null_mut()),
			);
		}
	}

	pub fn gen_uniform_grid_4d(
		&self,
		noise_out: &mut [f32],
		x_start: i32,
		y_start: i32,
		z_start: i32,
		w_start: i32,
		x_size: i32,
		y_size: i32,
		z_size: i32,
		w_size: i32,
		frequency: Frequency,
		seed: Seed,
		output_min_max: Option<&mut [f32; 2]>,
	) {
		assert!(noise_out.len() == (x_size * y_size * z_size * w_size).try_into().unwrap());
		unsafe {
			raw::fnGenUniformGrid4D(
				self.ptr,
				noise_out.as_mut_ptr(),
				x_start,
				y_start,
				z_start,
				w_start,
				x_size,
				y_size,
				z_size,
				w_size,
				frequency.0,
				seed.0,
				output_min_max
					.map(|s| s.as_mut_ptr())
					.unwrap_or(std::ptr::null_mut()),
			);
		}
	}

	pub fn gen_position_array_2d(
		&self,
		noise_out: &mut [f32],
		count: i32,
		x_pos_array: &[f32],
		y_pos_array: &[f32],
		x_offset: f32,
		y_offset: f32,
		seed: Seed,
		output_min_max: Option<&mut [f32; 2]>,
	) {
		assert!(noise_out.len() == count.try_into().unwrap());
		assert_eq!(
			x_pos_array.len() * y_pos_array.len(),
			count.try_into().unwrap()
		);
		assert!(x_pos_array.len() == y_pos_array.len());

		unsafe {
			raw::fnGenPositionArray2D(
				self.ptr,
				noise_out.as_mut_ptr(),
				count,
				x_pos_array.as_ptr(),
				y_pos_array.as_ptr(),
				x_offset,
				y_offset,
				seed.0,
				output_min_max
					.map(|s| s.as_mut_ptr())
					.unwrap_or(std::ptr::null_mut()),
			);
		}
	}

	pub fn gen_position_array_3d(
		&self,
		noise_out: &mut [f32],
		count: i32,
		x_pos_array: &[f32],
		y_pos_array: &[f32],
		z_pos_array: &[f32],
		x_offset: f32,
		y_offset: f32,
		z_offset: f32,
		seed: Seed,
		output_min_max: Option<&mut [f32; 2]>,
	) {
		assert!(noise_out.len() == count.try_into().unwrap());
		assert_eq!(
			x_pos_array.len() * y_pos_array.len() * z_pos_array.len(),
			count.try_into().unwrap()
		);
		assert!(x_pos_array.len() == y_pos_array.len());
		assert!(x_pos_array.len() == z_pos_array.len());

		unsafe {
			raw::fnGenPositionArray3D(
				self.ptr,
				noise_out.as_mut_ptr(),
				count,
				x_pos_array.as_ptr(),
				y_pos_array.as_ptr(),
				z_pos_array.as_ptr(),
				x_offset,
				y_offset,
				z_offset,
				seed.0,
				output_min_max
					.map(|s| s.as_mut_ptr())
					.unwrap_or(std::ptr::null_mut()),
			);
		}
	}

	pub fn gen_position_array_4d(
		&self,
		noise_out: &mut [f32],
		count: i32,
		x_pos_array: &[f32],
		y_pos_array: &[f32],
		z_pos_array: &[f32],
		w_pos_array: &[f32],
		x_offset: f32,
		y_offset: f32,
		z_offset: f32,
		w_offset: f32,
		seed: Seed,
		output_min_max: Option<&mut [f32; 2]>,
	) {
		assert!(noise_out.len() == count.try_into().unwrap());
		assert_eq!(
			x_pos_array.len() * y_pos_array.len() * z_pos_array.len() * w_pos_array.len(),
			count.try_into().unwrap()
		);
		assert!(x_pos_array.len() == y_pos_array.len());
		assert!(x_pos_array.len() == z_pos_array.len());
		assert!(x_pos_array.len() == w_pos_array.len());

		unsafe {
			raw::fnGenPositionArray4D(
				self.ptr,
				noise_out.as_mut_ptr(),
				count,
				x_pos_array.as_ptr(),
				y_pos_array.as_ptr(),
				z_pos_array.as_ptr(),
				w_pos_array.as_ptr(),
				x_offset,
				y_offset,
				z_offset,
				w_offset,
				seed.0,
				output_min_max
					.map(|s| s.as_mut_ptr())
					.unwrap_or(std::ptr::null_mut()),
			);
		}
	}

	pub fn gen_tileable_2d(
		&self,
		noise_out: &mut [f32],
		x_size: i32,
		y_size: i32,
		frequency: Frequency,
		seed: Seed,
		output_min_max: Option<&mut [f32; 2]>,
	) {
		assert!(noise_out.len() == (x_size * y_size).try_into().unwrap());

		unsafe {
			raw::fnGenTileable2D(
				self.ptr,
				noise_out.as_mut_ptr(),
				x_size,
				y_size,
				frequency.0,
				seed.0,
				output_min_max
					.map(|s| s.as_mut_ptr())
					.unwrap_or(std::ptr::null_mut()),
			);
		}
	}

	pub fn gen_single_2d(&self, x: f32, y: f32, seed: Seed) -> f32 {
		unsafe { raw::fnGenSingle2D(self.ptr, x, y, seed.0) }
	}

	pub fn gen_single_3d(&self, x: f32, y: f32, z: f32, seed: Seed) -> f32 {
		unsafe { raw::fnGenSingle3D(self.ptr, x, y, z, seed.0) }
	}

	pub fn gen_single_4d(&self, x: f32, y: f32, z: f32, w: f32, seed: Seed) -> f32 {
		unsafe { raw::fnGenSingle4D(self.ptr, x, y, z, w, seed.0) }
	}

	pub(crate) fn set_node_lookup(&mut self, node_lookup_index: NodeIndex, node: Node) {
		assert!(unsafe { raw::fnSetNodeLookup(self.ptr, node_lookup_index.0, node.ptr) });
		self.children.push(node);
	}

	pub(crate) fn set_variable_float(&mut self, variable_index: i32, value: f32) {
		assert!(unsafe { raw::fnSetVariableFloat(self.ptr, variable_index, value) });
	}

	pub(crate) fn set_variable_int_enum(&mut self, variable_index: i32, value: i32) {
		assert!(unsafe { raw::fnSetVariableIntEnum(self.ptr, variable_index, value) });
	}

	pub(crate) fn set_hybrid_node_lookup(&mut self, hybrid_index: HybridIndex, node: Node) {
		assert!(unsafe { raw::fnSetHybridNodeLookup(self.ptr, hybrid_index.0, node.ptr) });
		self.children.push(node);
	}

	pub(crate) fn set_hybrid_float(&mut self, hybrid_index: HybridIndex, value: f32) {
		assert!(unsafe { raw::fnSetHybridFloat(self.ptr, hybrid_index.0, value) })
	}
}

#[derive(Debug)]
pub enum ConstructionError {
	EncodedNodeTreeStringHasANulByte(std::ffi::NulError),
	NotAValidNodeTree,
	Unknown,
}

impl std::ops::Drop for Node {
	fn drop(&mut self) {
		unsafe { raw::fnDeleteNodeRef(self.ptr) };
	}
}

unsafe impl Send for Node {}
unsafe impl Sync for Node {}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_generator() {
		let generator = Node::new_from_tree("DQAFAAAAAAAAQAgAAAAAAD8AAAAAAA==").unwrap();
		let seed = Seed(0);
		let frequency = Frequency(1.0);
		println!("SimdLevel: {:?}", generator.simd_level());
		println!("Metadata id: {}", generator.metadata_id());

		{
			let mut f = [0.0; 4];
			let mut range = [0.0; 2];
			generator.gen_uniform_grid_2d(&mut f, 0, 0, 2, 2, frequency, seed, Some(&mut range));
			println!("uniform grid2d: {:?} (range: {:?})", f, range);
		}

		{
			let mut f = [0.0; 8];
			let mut range = [0.0; 2];
			generator.gen_uniform_grid_3d(
				&mut f,
				0,
				0,
				0,
				2,
				2,
				2,
				frequency,
				seed,
				Some(&mut range),
			);
			println!("uniform grid3d: {:?} (range: {:?})", f, range);
		}

		{
			let mut f = [0.0; 16];
			let mut range = [0.0; 2];
			generator.gen_uniform_grid_4d(
				&mut f,
				0,
				0,
				0,
				0,
				2,
				2,
				2,
				2,
				frequency,
				seed,
				Some(&mut range),
			);
			println!("uniform grid4d: {:?} (range: {:?})", f, range);
		}

		{
			let mut f = [0.0; 4];
			let mut range = [0.0; 2];
			generator.gen_position_array_2d(
				&mut f,
				4,
				&[0.0, 1.0],
				&[0.0, 1.0],
				0.0,
				0.0,
				seed,
				Some(&mut range),
			);
			println!("position array 2d: {:?} (range: {:?})", f, range);
		}

		{
			let mut f = [0.0; 8];
			let mut range = [0.0; 2];
			generator.gen_position_array_3d(
				&mut f,
				8,
				&[0.0, 1.0],
				&[0.0, 1.0],
				&[0.0, 1.0],
				0.0,
				0.0,
				0.0,
				seed,
				Some(&mut range),
			);
			println!("position array 3d: {:?} (range: {:?})", f, range);
		}

		{
			let mut f = [0.0; 16];
			let mut range = [0.0; 2];
			generator.gen_position_array_4d(
				&mut f,
				16,
				&[0.0, 1.0],
				&[0.0, 1.0],
				&[0.0, 1.0],
				&[0.0, 1.0],
				0.0,
				0.0,
				0.0,
				0.0,
				seed,
				Some(&mut range),
			);
			println!("position array 4d: {:?} (range: {:?})", f, range);
		}

		{
			let mut f = [0.0; 4];
			let mut range = [0.0; 2];
			generator.gen_tileable_2d(&mut f, 2, 2, frequency, seed, Some(&mut range));
			println!("tileable 2d: {:?} (range: {:?})", f, range);
		}

		println!("single_2d: {}", generator.gen_single_2d(0.1, 0.2, seed));
		println!(
			"single_3d: {}",
			generator.gen_single_3d(0.1, 0.2, 0.3, seed)
		);
		println!(
			"single_4d: {}",
			generator.gen_single_4d(0.1, 0.2, 0.3, 0.4, seed)
		);
	}

	#[test]
	fn test_interface() {
		let _generator = Node::new(NodeType::Simplex);
	}
}
