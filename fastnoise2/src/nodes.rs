use crate::{HybridIndex, Node, NodeIndex, NodeType};

/// Shorthand for calling Into<Node>
pub trait Buildable {
	fn build(self) -> Node;
}

impl<T> Buildable for T
where
	Node: From<T>,
{
	fn build(self) -> Node {
		self.into()
	}
}

#[derive(Default)]
pub struct Constant {
	pub value: f32,
}

impl Constant {
	pub fn new(value: f32) -> Self {
		Self { value }
	}
}

impl From<Constant> for Node {
	fn from(t: Constant) -> Node {
		let mut generator = Node::new(NodeType::Constant);
		generator.set_variable_float(0, t.value);
		generator
	}
}

#[derive(Default)]
pub struct White;

impl White {
	pub fn new() -> Self {
		Self
	}
}

impl From<White> for Node {
	fn from(_: White) -> Node {
		Node::new(NodeType::White)
	}
}

pub struct Checkerboard {
	pub size: f32,
}

impl Checkerboard {
	pub fn new(size: f32) -> Self {
		Self { size }
	}
}

impl From<Checkerboard> for Node {
	fn from(t: Checkerboard) -> Node {
		let mut generator = Node::new(NodeType::Checkerboard);
		generator.set_variable_float(0, t.size);
		generator
	}
}

pub struct SineWave {
	pub scale: f32,
}

impl SineWave {
	pub fn new(scale: f32) -> Self {
		Self { scale }
	}
}

impl From<SineWave> for Node {
	fn from(t: SineWave) -> Node {
		let mut generator = Node::new(NodeType::SineWave);
		generator.set_variable_float(0, t.scale);
		generator
	}
}

pub struct PositionOutput {
	pub multiplier_x: f32,
	pub multiplier_y: f32,
	pub multiplier_z: f32,
	pub multiplier_w: f32,
	pub offset_x: f32,
	pub offset_y: f32,
	pub offset_z: f32,
	pub offset_w: f32,
}

impl PositionOutput {
	pub fn new(multipliers: (f32, f32, f32, f32), offsets: (f32, f32, f32, f32)) -> Self {
		Self {
			multiplier_x: multipliers.0,
			multiplier_y: multipliers.1,
			multiplier_z: multipliers.2,
			multiplier_w: multipliers.3,
			offset_x: offsets.0,
			offset_y: offsets.1,
			offset_z: offsets.2,
			offset_w: offsets.3,
		}
	}
}

impl From<PositionOutput> for Node {
	fn from(t: PositionOutput) -> Node {
		let mut generator = Node::new(NodeType::PositionOutput);
		generator.set_variable_float(0, t.multiplier_x);
		generator.set_variable_float(1, t.multiplier_y);
		generator.set_variable_float(2, t.multiplier_z);
		generator.set_variable_float(3, t.multiplier_w);
		generator.set_variable_float(4, t.offset_x);
		generator.set_variable_float(5, t.offset_y);
		generator.set_variable_float(6, t.offset_z);
		generator.set_variable_float(7, t.offset_w);
		generator
	}
}

#[derive(Copy, Clone)]
pub enum DistanceFunction {
	Euclidean,
	EuclidianSquared,
	Manhattan,
	Hybrid,
	MaxAxis,
}

pub struct DistanceToPoint {
	pub distance_function: DistanceFunction,
	pub point_x: f32,
	pub point_y: f32,
	pub point_z: f32,
	pub point_w: f32,
}

impl DistanceToPoint {
	pub fn new(distance_function: DistanceFunction, point: (f32, f32, f32, f32)) -> Self {
		Self {
			distance_function,
			point_x: point.0,
			point_y: point.1,
			point_z: point.2,
			point_w: point.3,
		}
	}
}

impl From<DistanceToPoint> for Node {
	fn from(t: DistanceToPoint) -> Node {
		let mut generator = Node::new(NodeType::DistanceToPoint);
		generator.set_variable_int_enum(0, t.distance_function as i32);
		generator.set_variable_float(1, t.point_x);
		generator.set_variable_float(2, t.point_y);
		generator.set_variable_float(3, t.point_z);
		generator.set_variable_float(4, t.point_w);
		generator
	}
}

#[derive(Default)]
pub struct Value;

impl Value {
	pub fn new() -> Self {
		Self
	}
}

impl From<Value> for Node {
	fn from(_: Value) -> Node {
		Node::new(NodeType::Value)
	}
}

#[derive(Default)]
pub struct Perlin;

impl Perlin {
	pub fn new() -> Self {
		Self
	}
}

impl From<Perlin> for Node {
	fn from(_: Perlin) -> Node {
		Node::new(NodeType::Perlin)
	}
}

#[derive(Default)]
pub struct Simplex;

impl Simplex {
	pub fn new() -> Self {
		Self
	}
}

impl From<Simplex> for Node {
	fn from(_: Simplex) -> Node {
		Node::new(NodeType::Simplex)
	}
}

#[derive(Default)]
pub struct OpenSimplex2;

impl OpenSimplex2 {
	pub fn new() -> Self {
		Self
	}
}

impl From<OpenSimplex2> for Node {
	fn from(_: OpenSimplex2) -> Node {
		Node::new(NodeType::OpenSimplex2)
	}
}

pub enum HybridFloat {
	Float(f32),
	Node(Node),
}

impl HybridFloat {
	pub fn set(self, node: &mut Node, index: HybridIndex) {
		match self {
			HybridFloat::Float(v) => node.set_hybrid_float(index, v),
			HybridFloat::Node(v) => node.set_hybrid_node_lookup(index, v),
		};
	}
}

impl From<f32> for HybridFloat {
	fn from(f: f32) -> Self {
		Self::Float(f)
	}
}

impl<T> From<T> for HybridFloat
where
	T: Into<Node>,
{
	fn from(node: T) -> Self {
		Self::Node(node.into())
	}
}

pub struct CellularValue {
	pub distance_function: DistanceFunction,
	pub value_index: i32,
	pub jitter_modifier: HybridFloat,
}

impl CellularValue {
	pub fn new(
		distance_function: DistanceFunction,
		value_index: i32,
		jitter_modifier: impl Into<HybridFloat>,
	) -> Self {
		Self {
			distance_function,
			value_index,
			jitter_modifier: jitter_modifier.into(),
		}
	}
}

impl From<CellularValue> for Node {
	fn from(t: CellularValue) -> Node {
		let mut generator = Node::new(NodeType::CellularValue);
		generator.set_variable_int_enum(0, t.distance_function as i32);
		generator.set_variable_int_enum(1, t.value_index);
		t.jitter_modifier.set(&mut generator, HybridIndex(0));
		generator
	}
}

#[derive(Copy, Clone)]
pub enum CellularDistanceReturnType {
	Index0,
	Index0Add1,
	Index0Sub1,
	Index0Mul1,
	Index0Div1,
}

pub struct CellularDistance {
	pub distance_function: DistanceFunction,
	pub distance_index_0: i32,
	pub distance_index_1: i32,
	pub return_type: CellularDistanceReturnType,
}

impl CellularDistance {
	pub fn new(
		distance_function: DistanceFunction,
		distance_index_0: i32,
		distance_index_1: i32,
		return_type: CellularDistanceReturnType,
	) -> Self {
		Self {
			distance_function,
			distance_index_0,
			distance_index_1,
			return_type,
		}
	}
}

impl From<CellularDistance> for Node {
	fn from(t: CellularDistance) -> Node {
		let mut generator = Node::new(NodeType::CellularDistance);
		generator.set_variable_int_enum(0, t.distance_function as i32);
		generator.set_variable_int_enum(1, t.distance_index_0);
		generator.set_variable_int_enum(2, t.distance_index_1);
		generator.set_variable_int_enum(3, t.return_type as i32);
		generator
	}
}

pub struct CellularLookup {
	pub distance_function: DistanceFunction,
	pub lookup_frequency: f32,
	pub lookup: Node,
	pub jitter_modifier: HybridFloat,
}

impl CellularLookup {
	pub fn new(
		distance_function: DistanceFunction,
		lookup_frequency: f32,
		lookup: impl Into<Node>,
		jitter_modifier: impl Into<HybridFloat>,
	) -> Self {
		Self {
			distance_function,
			lookup_frequency,
			lookup: lookup.into(),
			jitter_modifier: jitter_modifier.into(),
		}
	}
}

impl From<CellularLookup> for Node {
	fn from(t: CellularLookup) -> Node {
		let mut generator = Node::new(NodeType::CellularLookup);
		generator.set_variable_int_enum(0, t.distance_function as i32);
		generator.set_variable_float(1, t.lookup_frequency);
		generator.set_node_lookup(NodeIndex(0), t.lookup);
		t.jitter_modifier.set(&mut generator, HybridIndex(0));
		generator
	}
}

pub struct FractalFBm {
	pub source: Node,
	pub gain: HybridFloat,
	pub weighted_strength: HybridFloat,
	pub octaves: i32,
	pub lacunarity: f32,
}

impl FractalFBm {
	pub fn new(
		source: impl Into<Node>,
		gain: impl Into<HybridFloat>,
		weighted_strength: impl Into<HybridFloat>,
		octaves: i32,
		lacunarity: f32,
	) -> Self {
		Self {
			source: source.into(),
			gain: gain.into(),
			weighted_strength: weighted_strength.into(),
			octaves,
			lacunarity,
		}
	}
}

impl From<FractalFBm> for Node {
	fn from(t: FractalFBm) -> Node {
		let mut generator = Node::new(NodeType::FractalFBm);
		generator.set_node_lookup(NodeIndex(0), t.source);
		t.gain.set(&mut generator, HybridIndex(0));
		t.weighted_strength.set(&mut generator, HybridIndex(1));
		generator.set_variable_int_enum(0, t.octaves);
		generator.set_variable_float(1, t.lacunarity);
		generator
	}
}

pub struct FractalPingPong {
	pub source: Node,
	pub gain: HybridFloat,
	pub weighted_strength: HybridFloat,
	pub ping_pong_strength: HybridFloat,
	pub octaves: i32,
	pub lacunarity: f32,
}

impl FractalPingPong {
	pub fn new(
		source: impl Into<Node>,
		gain: impl Into<HybridFloat>,
		weighted_strength: impl Into<HybridFloat>,
		ping_pong_strength: impl Into<HybridFloat>,
		octaves: i32,
		lacunarity: f32,
	) -> Self {
		Self {
			source: source.into(),
			gain: gain.into(),
			weighted_strength: weighted_strength.into(),
			ping_pong_strength: ping_pong_strength.into(),
			octaves,
			lacunarity,
		}
	}
}

impl From<FractalPingPong> for Node {
	fn from(t: FractalPingPong) -> Node {
		let mut generator = Node::new(NodeType::FractalPingPong);
		generator.set_node_lookup(NodeIndex(0), t.source);
		t.gain.set(&mut generator, HybridIndex(0));
		t.weighted_strength.set(&mut generator, HybridIndex(1));
		t.ping_pong_strength.set(&mut generator, HybridIndex(2));
		generator.set_variable_int_enum(0, t.octaves);
		generator.set_variable_float(1, t.lacunarity);
		generator
	}
}

pub struct FractalRidged {
	pub source: Node,
	pub gain: HybridFloat,
	pub weighted_strength: HybridFloat,
	pub octaves: i32,
	pub lacunarity: f32,
}

impl FractalRidged {
	pub fn new(
		source: impl Into<Node>,
		gain: impl Into<HybridFloat>,
		weighted_strength: impl Into<HybridFloat>,
		octaves: i32,
		lacunarity: f32,
	) -> Self {
		Self {
			source: source.into(),
			gain: gain.into(),
			weighted_strength: weighted_strength.into(),
			octaves,
			lacunarity,
		}
	}

	pub fn build(self) -> Node {
		let mut generator = Node::new(NodeType::FractalRidged);
		generator.set_node_lookup(NodeIndex(0), self.source);
		self.gain.set(&mut generator, HybridIndex(0));
		self.weighted_strength.set(&mut generator, HybridIndex(1));
		generator.set_variable_int_enum(0, self.octaves);
		generator.set_variable_float(1, self.lacunarity);
		generator
	}
}

impl From<FractalRidged> for Node {
	fn from(t: FractalRidged) -> Node {
		let mut generator = Node::new(NodeType::FractalRidged);
		generator.set_node_lookup(NodeIndex(0), t.source);
		t.gain.set(&mut generator, HybridIndex(0));
		t.weighted_strength.set(&mut generator, HybridIndex(1));
		generator.set_variable_int_enum(0, t.octaves);
		generator.set_variable_float(1, t.lacunarity);
		generator
	}
}

pub struct DomainWarpGradient {
	pub source: Node,
	pub warp_amplitude: HybridFloat,
	pub warp_frequency: f32,
}

impl DomainWarpGradient {
	pub fn new(
		source: impl Into<Node>,
		warp_amplitude: impl Into<HybridFloat>,
		warp_frequency: f32,
	) -> Self {
		Self {
			source: source.into(),
			warp_amplitude: warp_amplitude.into(),
			warp_frequency,
		}
	}
}

impl From<DomainWarpGradient> for Node {
	fn from(t: DomainWarpGradient) -> Node {
		let mut generator = Node::new(NodeType::DomainWarpGradient);
		generator.set_node_lookup(NodeIndex(0), t.source);
		t.warp_amplitude.set(&mut generator, HybridIndex(0));
		generator.set_variable_float(0, t.warp_frequency);
		generator
	}
}

pub struct DomainWarpFractalProgressive {
	pub domain_warp_source: Node,
	pub gain: HybridFloat,
	pub weighted_strength: HybridFloat,
	pub octaves: i32,
	pub lacunarity: f32,
}

impl DomainWarpFractalProgressive {
	pub fn new(
		domain_warp_source: impl Into<Node>,
		gain: impl Into<HybridFloat>,
		weighted_strength: impl Into<HybridFloat>,
		octaves: i32,
		lacunarity: f32,
	) -> Self {
		Self {
			domain_warp_source: domain_warp_source.into(),
			gain: gain.into(),
			weighted_strength: weighted_strength.into(),
			octaves,
			lacunarity,
		}
	}
}

impl From<DomainWarpFractalProgressive> for Node {
	fn from(t: DomainWarpFractalProgressive) -> Node {
		let mut generator = Node::new(NodeType::DomainWarpFractalProgressive);
		generator.set_node_lookup(NodeIndex(0), t.domain_warp_source);
		t.gain.set(&mut generator, HybridIndex(0));
		t.weighted_strength.set(&mut generator, HybridIndex(1));
		generator.set_variable_int_enum(0, t.octaves);
		generator.set_variable_float(1, t.lacunarity);
		generator
	}
}

pub struct DomainWarpFractalIndependant {
	pub domain_warp_source: Node,
	pub gain: HybridFloat,
	pub weighted_strength: HybridFloat,
	pub octaves: i32,
	pub lacunarity: f32,
}

impl DomainWarpFractalIndependant {
	pub fn new(
		domain_warp_source: impl Into<Node>,
		gain: impl Into<HybridFloat>,
		weighted_strength: impl Into<HybridFloat>,
		octaves: i32,
		lacunarity: f32,
	) -> Self {
		Self {
			domain_warp_source: domain_warp_source.into(),
			gain: gain.into(),
			weighted_strength: weighted_strength.into(),
			octaves,
			lacunarity,
		}
	}
}

impl From<DomainWarpFractalIndependant> for Node {
	fn from(t: DomainWarpFractalIndependant) -> Node {
		let mut generator = Node::new(NodeType::DomainWarpFractalIndependant);
		generator.set_node_lookup(NodeIndex(0), t.domain_warp_source);
		t.gain.set(&mut generator, HybridIndex(0));
		t.weighted_strength.set(&mut generator, HybridIndex(1));
		generator.set_variable_int_enum(0, t.octaves);
		generator.set_variable_float(1, t.lacunarity);
		generator
	}
}

pub struct DomainScale {
	pub source: Node,
	pub scale: f32,
}

impl DomainScale {
	pub fn new(source: impl Into<Node>, scale: f32) -> Self {
		Self {
			source: source.into(),
			scale,
		}
	}
}

impl From<DomainScale> for Node {
	fn from(t: DomainScale) -> Node {
		let mut generator = Node::new(NodeType::DomainScale);
		generator.set_node_lookup(NodeIndex(0), t.source);
		generator.set_variable_float(0, t.scale);
		generator
	}
}

pub struct DomainOffset {
	pub source: Node,
	pub x_offset: HybridFloat,
	pub y_offset: HybridFloat,
	pub z_offset: HybridFloat,
	pub w_offset: HybridFloat,
}

impl DomainOffset {
	pub fn new(
		source: impl Into<Node>,
		x_offset: impl Into<HybridFloat>,
		y_offset: impl Into<HybridFloat>,
		z_offset: impl Into<HybridFloat>,
		w_offset: impl Into<HybridFloat>,
	) -> Self {
		Self {
			source: source.into(),
			x_offset: x_offset.into(),
			y_offset: y_offset.into(),
			z_offset: z_offset.into(),
			w_offset: w_offset.into(),
		}
	}
}

impl From<DomainOffset> for Node {
	fn from(t: DomainOffset) -> Node {
		let mut generator = Node::new(NodeType::DomainOffset);
		generator.set_node_lookup(NodeIndex(0), t.source);
		t.x_offset.set(&mut generator, HybridIndex(0));
		t.y_offset.set(&mut generator, HybridIndex(1));
		t.z_offset.set(&mut generator, HybridIndex(2));
		t.w_offset.set(&mut generator, HybridIndex(3));
		generator
	}
}

pub struct DomainRotate {
	pub source: Node,
	pub yaw: f32,
	pub pitch: f32,
	pub roll: f32,
}

impl DomainRotate {
	pub fn new(source: impl Into<Node>, yaw: f32, pitch: f32, roll: f32) -> Self {
		Self {
			source: source.into(),
			yaw,
			pitch,
			roll,
		}
	}
}

impl From<DomainRotate> for Node {
	fn from(t: DomainRotate) -> Node {
		let mut generator = Node::new(NodeType::DomainRotate);
		generator.set_node_lookup(NodeIndex(0), t.source);
		generator.set_variable_float(0, t.yaw);
		generator.set_variable_float(1, t.pitch);
		generator.set_variable_float(2, t.roll);
		generator
	}
}

pub struct SeedOffset {
	pub source: Node,
	pub offset: i32,
}

impl SeedOffset {
	pub fn new(source: impl Into<Node>, offset: i32) -> Self {
		Self {
			source: source.into(),
			offset,
		}
	}
}

impl From<SeedOffset> for Node {
	fn from(t: SeedOffset) -> Node {
		let mut generator = Node::new(NodeType::SeedOffset);
		generator.set_node_lookup(NodeIndex(0), t.source);
		generator.set_variable_int_enum(0, t.offset);
		generator
	}
}

pub struct Remap {
	pub source: Node,
	pub from_min: f32,
	pub from_max: f32,
	pub to_min: f32,
	pub to_max: f32,
}

impl Remap {
	pub fn new(
		source: impl Into<Node>,
		from_min: f32,
		from_max: f32,
		to_min: f32,
		to_max: f32,
	) -> Self {
		Self {
			source: source.into(),
			from_min,
			from_max,
			to_min,
			to_max,
		}
	}
}

impl From<Remap> for Node {
	fn from(t: Remap) -> Node {
		let mut generator = Node::new(NodeType::Remap);
		generator.set_node_lookup(NodeIndex(0), t.source);
		generator.set_variable_float(0, t.from_min);
		generator.set_variable_float(1, t.from_max);
		generator.set_variable_float(2, t.to_min);
		generator.set_variable_float(3, t.to_max);
		generator
	}
}

pub struct ConvertRGBA8 {
	pub source: Node,
	pub min: f32,
	pub max: f32,
}

impl ConvertRGBA8 {
	pub fn new(source: impl Into<Node>, min: f32, max: f32) -> Self {
		Self {
			source: source.into(),
			min,
			max,
		}
	}
}

impl From<ConvertRGBA8> for Node {
	fn from(t: ConvertRGBA8) -> Node {
		let mut generator = Node::new(NodeType::ConvertRGBA8);
		generator.set_node_lookup(NodeIndex(0), t.source);
		generator.set_variable_float(0, t.min);
		generator.set_variable_float(1, t.max);
		generator
	}
}

pub struct Add {
	pub lhs: Node,
	pub rhs: HybridFloat,
}

impl Add {
	pub fn new(lhs: impl Into<Node>, rhs: impl Into<HybridFloat>) -> Self {
		Self {
			lhs: lhs.into(),
			rhs: rhs.into(),
		}
	}
}

impl From<Add> for Node {
	fn from(t: Add) -> Node {
		let mut generator = Node::new(NodeType::Add);
		generator.set_node_lookup(NodeIndex(0), t.lhs);
		t.rhs.set(&mut generator, HybridIndex(0));
		generator
	}
}

pub struct Subtract {
	pub lhs: HybridFloat,
	pub rhs: HybridFloat,
}

impl Subtract {
	pub fn new(lhs: impl Into<HybridFloat>, rhs: impl Into<HybridFloat>) -> Self {
		Self {
			lhs: lhs.into(),
			rhs: rhs.into(),
		}
	}
}

impl From<Subtract> for Node {
	fn from(t: Subtract) -> Node {
		let mut generator = Node::new(NodeType::Subtract);
		t.lhs.set(&mut generator, HybridIndex(0));
		t.rhs.set(&mut generator, HybridIndex(1));
		generator
	}
}

pub struct Multiply {
	pub lhs: Node,
	pub rhs: HybridFloat,
}

impl Multiply {
	pub fn new(lhs: impl Into<Node>, rhs: impl Into<HybridFloat>) -> Self {
		Self {
			lhs: lhs.into(),
			rhs: rhs.into(),
		}
	}
}

impl From<Multiply> for Node {
	fn from(t: Multiply) -> Node {
		let mut generator = Node::new(NodeType::Multiply);
		generator.set_node_lookup(NodeIndex(0), t.lhs);
		t.rhs.set(&mut generator, HybridIndex(0));
		generator
	}
}

pub struct Divide {
	pub lhs: HybridFloat,
	pub rhs: HybridFloat,
}

impl Divide {
	pub fn new(lhs: impl Into<HybridFloat>, rhs: impl Into<HybridFloat>) -> Self {
		Self {
			lhs: lhs.into(),
			rhs: rhs.into(),
		}
	}
}

impl From<Divide> for Node {
	fn from(t: Divide) -> Node {
		let mut generator = Node::new(NodeType::Divide);
		t.lhs.set(&mut generator, HybridIndex(0));
		t.rhs.set(&mut generator, HybridIndex(1));
		generator
	}
}

pub struct Min {
	pub lhs: Node,
	pub rhs: HybridFloat,
}

impl Min {
	pub fn new(lhs: impl Into<Node>, rhs: impl Into<HybridFloat>) -> Self {
		Self {
			lhs: lhs.into(),
			rhs: rhs.into(),
		}
	}
}

impl From<Min> for Node {
	fn from(t: Min) -> Node {
		let mut generator = Node::new(NodeType::Min);
		generator.set_node_lookup(NodeIndex(0), t.lhs);
		t.rhs.set(&mut generator, HybridIndex(0));
		generator
	}
}

pub struct Max {
	pub lhs: Node,
	pub rhs: HybridFloat,
}

impl Max {
	pub fn new(lhs: impl Into<Node>, rhs: impl Into<HybridFloat>) -> Self {
		Self {
			lhs: lhs.into(),
			rhs: rhs.into(),
		}
	}
}

impl From<Max> for Node {
	fn from(t: Max) -> Node {
		let mut generator = Node::new(NodeType::Max);
		generator.set_node_lookup(NodeIndex(0), t.lhs);
		t.rhs.set(&mut generator, HybridIndex(0));
		generator
	}
}

pub struct MinSmooth {
	pub lhs: Node,
	pub rhs: HybridFloat,
	pub smoothness: HybridFloat,
}

impl MinSmooth {
	pub fn new(
		lhs: impl Into<Node>,
		rhs: impl Into<HybridFloat>,
		smoothness: impl Into<HybridFloat>,
	) -> Self {
		Self {
			lhs: lhs.into(),
			rhs: rhs.into(),
			smoothness: smoothness.into(),
		}
	}
}

impl From<MinSmooth> for Node {
	fn from(t: MinSmooth) -> Node {
		let mut generator = Node::new(NodeType::MinSmooth);
		generator.set_node_lookup(NodeIndex(0), t.lhs);
		t.rhs.set(&mut generator, HybridIndex(0));
		t.smoothness.set(&mut generator, HybridIndex(1));
		generator
	}
}

pub struct MaxSmooth {
	pub lhs: Node,
	pub rhs: HybridFloat,
	pub smoothness: HybridFloat,
}

impl MaxSmooth {
	pub fn new(
		lhs: impl Into<Node>,
		rhs: impl Into<HybridFloat>,
		smoothness: impl Into<HybridFloat>,
	) -> Self {
		Self {
			lhs: lhs.into(),
			rhs: rhs.into(),
			smoothness: smoothness.into(),
		}
	}
}

impl From<MaxSmooth> for Node {
	fn from(t: MaxSmooth) -> Node {
		let mut generator = Node::new(NodeType::MaxSmooth);
		generator.set_node_lookup(NodeIndex(0), t.lhs);
		t.rhs.set(&mut generator, HybridIndex(0));
		t.smoothness.set(&mut generator, HybridIndex(1));
		generator
	}
}

pub struct Fade {
	pub a: Node,
	pub b: Node,
	pub fade: HybridFloat,
}

impl Fade {
	pub fn new(a: impl Into<Node>, b: impl Into<Node>, fade: impl Into<HybridFloat>) -> Self {
		Self {
			a: a.into(),
			b: b.into(),
			fade: fade.into(),
		}
	}
}

impl From<Fade> for Node {
	fn from(t: Fade) -> Node {
		let mut generator = Node::new(NodeType::Fade);
		generator.set_node_lookup(NodeIndex(0), t.a);
		generator.set_node_lookup(NodeIndex(1), t.b);
		t.fade.set(&mut generator, HybridIndex(0));
		generator
	}
}

pub struct Terrace {
	pub source: Node,
	pub multiplier: f32,
	pub smoothness: f32,
}

impl Terrace {
	pub fn new(source: impl Into<Node>, multiplier: f32, smoothness: f32) -> Self {
		Self {
			source: source.into(),
			multiplier,
			smoothness,
		}
	}
}

impl From<Terrace> for Node {
	fn from(t: Terrace) -> Node {
		let mut generator = Node::new(NodeType::Terrace);
		generator.set_node_lookup(NodeIndex(0), t.source);
		generator.set_variable_float(0, t.multiplier);
		generator.set_variable_float(1, t.smoothness);
		generator
	}
}

pub struct PowFloat {
	pub value: HybridFloat,
	pub pow: HybridFloat,
}

impl PowFloat {
	pub fn new(value: impl Into<HybridFloat>, pow: impl Into<HybridFloat>) -> Self {
		Self {
			value: value.into(),
			pow: pow.into(),
		}
	}
}

impl From<PowFloat> for Node {
	fn from(t: PowFloat) -> Node {
		let mut generator = Node::new(NodeType::PowFloat);
		t.value.set(&mut generator, HybridIndex(0));
		t.pow.set(&mut generator, HybridIndex(1));
		generator
	}
}

pub struct PowInt {
	pub value: Node,
	pub pow: i32,
}

impl PowInt {
	pub fn new(value: impl Into<Node>, pow: i32) -> Self {
		Self {
			value: value.into(),
			pow,
		}
	}
}

impl From<PowInt> for Node {
	fn from(t: PowInt) -> Node {
		let mut generator = Node::new(NodeType::PowInt);
		generator.set_node_lookup(NodeIndex(0), t.value);
		generator.set_variable_int_enum(0, t.pow);
		generator
	}
}

pub struct DomainAxisScale {
	pub source: Node,
	pub scale_x: f32,
	pub scale_y: f32,
	pub scale_z: f32,
	pub scale_w: f32,
}

impl DomainAxisScale {
	pub fn new(source: impl Into<Node>, scale: (f32, f32, f32, f32)) -> Self {
		Self {
			source: source.into(),
			scale_x: scale.0,
			scale_y: scale.1,
			scale_z: scale.2,
			scale_w: scale.3,
		}
	}
}

impl From<DomainAxisScale> for Node {
	fn from(t: DomainAxisScale) -> Node {
		let mut generator = Node::new(NodeType::DomainAxisScale);
		generator.set_node_lookup(NodeIndex(0), t.source);
		generator.set_variable_float(0, t.scale_x);
		generator.set_variable_float(1, t.scale_y);
		generator.set_variable_float(2, t.scale_y);
		generator.set_variable_float(3, t.scale_z);
		generator
	}
}

pub struct AddDimension {
	pub source: Node,
	pub new_dimension_position: HybridFloat,
}

impl AddDimension {
	pub fn new(source: impl Into<Node>, new_dimension_position: impl Into<HybridFloat>) -> Self {
		Self {
			source: source.into(),
			new_dimension_position: new_dimension_position.into(),
		}
	}
}

impl From<AddDimension> for Node {
	fn from(t: AddDimension) -> Node {
		let mut generator = Node::new(NodeType::AddDimension);
		generator.set_node_lookup(NodeIndex(0), t.source);
		t.new_dimension_position.set(&mut generator, HybridIndex(0));
		generator
	}
}

#[derive(Debug, Copy, Clone)]
pub enum Dimension {
	X,
	Y,
	Z,
	W,
}

pub struct RemoveDimension {
	pub source: Node,
	pub remove_dimension: Dimension,
}

impl RemoveDimension {
	pub fn new(source: impl Into<Node>, remove_dimension: Dimension) -> Self {
		Self {
			source: source.into(),
			remove_dimension,
		}
	}
}

impl From<RemoveDimension> for Node {
	fn from(t: RemoveDimension) -> Node {
		let mut generator = Node::new(NodeType::RemoveDimension);
		generator.set_node_lookup(NodeIndex(0), t.source);
		generator.set_variable_int_enum(0, t.remove_dimension as i32);
		generator
	}
}

pub struct GeneratorCache {
	pub source: Node,
}

impl GeneratorCache {
	pub fn new(source: impl Into<Node>) -> Self {
		Self {
			source: source.into(),
		}
	}
}

impl From<GeneratorCache> for Node {
	fn from(t: GeneratorCache) -> Node {
		let mut generator = Node::new(NodeType::GeneratorCache);
		generator.set_node_lookup(NodeIndex(0), t.source);
		generator
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::{NodeType, Seed};

	#[test]
	fn validate_nodetype() {
		for (index, id) in NodeType::iter().enumerate() {
			println!("{} = {},", id.name(), index);
			assert_eq!(
				id.name(),
				format!("{:?}", NodeType::try_from(index).unwrap())
			)
		}
	}

	#[test]
	fn test_node_constant() {
		let node = Constant::new(6.0).build();
		let f = node.gen_single_2d(0.0, 0.0, Seed(0));
		assert_eq!(f, 6.0);
	}

	#[test]
	fn test_node_white() {
		let node = White.build();
		let _ = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_checkerboard() {
		let node = Checkerboard::new(1.0).build();
		let f = node.gen_single_2d(0.0, 0.0, Seed(0));
		assert!(f == 0.0 || f == 1.0);
	}

	#[test]
	fn test_node_sinewave() {
		let node = SineWave::new(1.0).build();
		let f = node.gen_single_2d(0.0, 0.0, Seed(0));
		assert!((0.0..=1.0).contains(&f));
	}

	#[test]
	fn test_node_position_output() {
		let node = PositionOutput::new((0.0, 0.0, 0.0, 0.0), (0.0, 0.0, 0.0, 0.0)).build();
		let f = node.gen_single_2d(0.0, 0.0, Seed(0));
		assert_eq!(f, 0.0);
	}

	#[test]
	fn test_node_distance_to_point() {
		let node = DistanceToPoint::new(DistanceFunction::MaxAxis, (1.0, 2.0, 3.0, 4.0)).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_value() {
		let node = Value.build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_perlin() {
		let node = Perlin.build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_simplex() {
		let node = Simplex.build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_open_simplex2() {
		let node = OpenSimplex2.build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_cellular_value() {
		let node = CellularValue::new(DistanceFunction::MaxAxis, 1, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_cellular_distance() {
		let node = CellularDistance::new(
			DistanceFunction::MaxAxis,
			0,
			1,
			CellularDistanceReturnType::Index0Div1,
		)
		.build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_cellular_lookup() {
		let node = CellularLookup::new(DistanceFunction::MaxAxis, 0.1, Simplex, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_fractal_fbm() {
		let node = FractalFBm::new(Simplex, 1.0, 1.0, 3, 2.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_fractal_ping_pong() {
		let node = FractalPingPong::new(Simplex, 1.0, 1.0, 1.0, 3, 2.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_fractal_ridged() {
		let node = FractalRidged::new(Simplex, 1.0, 1.0, 3, 2.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_domain_warp_gradient() {
		let node = DomainWarpGradient::new(Simplex, 1.0, 0.5).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_domain_warp_progressively() {
		let node = DomainWarpFractalProgressive::new(
			DomainWarpGradient::new(Simplex, 1.0, 0.5),
			1.0,
			1.0,
			3,
			2.0,
		)
		.build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_domain_warp_independant() {
		let node = DomainWarpFractalIndependant::new(
			DomainWarpGradient::new(Simplex, 1.0, 0.5),
			1.0,
			1.0,
			3,
			2.0,
		)
		.build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_domain_scale() {
		let node = DomainScale::new(Simplex, 1.5).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_domain_offset() {
		let node = DomainOffset::new(Simplex, Simplex, 1.0, 1.0, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_domain_rotate() {
		let node = DomainRotate::new(Simplex, 1.0, 0.0, 0.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_seed_offset() {
		let node = SeedOffset::new(Simplex, 1).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_remap() {
		let node = Remap::new(Simplex, 0.0, 1.0, 2.0, 3.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_convert_rgba8() {
		let node = ConvertRGBA8::new(Simplex, 0.0, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_add() {
		let node = Add::new(Simplex, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_subtract() {
		let node = Subtract::new(Simplex, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_multiply() {
		let node = Multiply::new(Simplex, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_divide() {
		let node = Divide::new(Simplex, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_min() {
		let node = Min::new(Simplex, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_max() {
		let node = Min::new(Simplex, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_min_smooth() {
		let node = MinSmooth::new(Simplex, 1.0, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_max_smooth() {
		let node = MinSmooth::new(Simplex, 1.0, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_fade() {
		let node = Fade::new(Simplex, Simplex, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_terrace() {
		let node = Terrace::new(Simplex, 1.0, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_pow_float() {
		let node = PowFloat::new(10.0, 2.5).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_pow_int() {
		let node = PowInt::new(Simplex, 2).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_domain_axis_scale() {
		let node = DomainAxisScale::new(Simplex, (1.0, 2.0, 3.0, 4.0)).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_add_dimension() {
		let node = AddDimension::new(Simplex, 1.0).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_remove_dimension() {
		let node = RemoveDimension::new(Simplex, Dimension::Y).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}

	#[test]
	fn test_node_generator_cache() {
		let node = GeneratorCache::new(Simplex).build();
		let _f = node.gen_single_2d(0.0, 0.0, Seed(0));
	}
}
