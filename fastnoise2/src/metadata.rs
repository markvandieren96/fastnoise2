mod meta_enum;
mod meta_variable;
mod node_type;

pub use meta_enum::*;
pub use meta_variable::*;
pub use node_type::*;

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn print_node_type_variables() {
		println!("NodeType count: {}", NodeType::count());
		for id in NodeType::iter() {
			println!(
				"\tNodeType: {}, variable_count: {}",
				id.name(),
				id.variable_count()
			);
			for var_i in id.iter_variables() {
				println!(
					"\t\tvariable: {}, type: {:?}, dim: {:?}",
					var_i.name(),
					var_i.ty(),
					var_i.dimension()
				);

				for enum_i in var_i.iter_enum() {
					println!("\t\t\tenum({}): {}", enum_i.index(), enum_i.name());
				}
			}
		}
	}

	#[test]
	fn print_nodetype_members() {
		for id in NodeType::iter() {
			println!(
				"pub enum {} {{ {} }}",
				id.name(),
				id.iter_variables()
					.map(|v| v.name().replace(' ', ""))
					.collect::<Vec<_>>()
					.join(", ")
			);
		}
	}
}
