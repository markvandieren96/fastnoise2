#[repr(u32)]
#[derive(Debug, Copy, Clone)]
pub enum SimdLevel {
	Null = 0,        // Uninitilised
	Scalar = 1 << 0, // 80386 instruction set (Not SIMD)
	Sse = 1 << 1,    // SSE (XMM) supported by CPU (not testing for O.S. support)
	Sse2 = 1 << 2,   // SSE2
	Sse3 = 1 << 3,   // SSE3
	SseE3 = 1 << 4,  // Supplementary SSE3 (SSSE3)
	Sse41 = 1 << 5,  // SSE4.1
	Sse42 = 1 << 6,  // SSE4.2
	Avx = 1 << 7,    // AVX supported by CPU and operating system
	Avx2 = 1 << 8,   // AVX2
	Avx512 = 1 << 9, // AVX512, AVX512DQ supported by CPU and operating system
	Neon = 1 << 16,  // ARM NEON
}

impl From<u32> for SimdLevel {
	fn from(level: u32) -> Self {
		match level {
			0 => SimdLevel::Null,
			1 => Self::Scalar,   // 80386 instruction set (Not SIMD)
			2 => Self::Sse,      // SSE (XMM) supported by CPU (not testing for O.S. support)
			4 => Self::Sse2,     // SSE2
			8 => Self::Sse3,     // SSE3
			16 => Self::SseE3,   // Supplementary SSE3 (SSSE3)
			32 => Self::Sse41,   // SSE4.1
			64 => Self::Sse42,   // SSE4.2
			128 => Self::Avx,    // AVX supported by CPU and operating system
			256 => Self::Avx2,   // AVX2
			512 => Self::Avx512, // AVX512, AVX512DQ supported by CPU and operating system
			65536 => Self::Neon, // ARM NEON
			_ => panic!("Can't convert u32 \"{}\" to SimdLevel", level),
		}
	}
}
