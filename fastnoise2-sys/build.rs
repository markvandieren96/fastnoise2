fn main() {
	let mut build = cc::Build::new();
	build
		.include("src/FastNoise2/include/")
		.include("src/FastNoise2/src/FastNoise/")
		.include("src/")
		.file("src/FastNoise2/src/FastNoise/FastNoise_C.cpp")
		.file("src/FastNoise2/src/FastNoise/Metadata.cpp")
		.file("src/FastNoise2/src/FastNoise/SmartNode.cpp")
		.file("src/FastNoise2/src/FastSIMD/FastSIMD_Level_AVX2.cpp")
		.file("src/FastNoise2/src/FastSIMD/FastSIMD_Level_AVX512.cpp")
		.file("src/FastNoise2/src/FastSIMD/FastSIMD_Level_NEON.cpp")
		.file("src/FastNoise2/src/FastSIMD/FastSIMD_Level_Scalar.cpp")
		.file("src/FastNoise2/src/FastSIMD/FastSIMD_Level_SSE2.cpp")
		.file("src/FastNoise2/src/FastSIMD/FastSIMD_Level_SSE3.cpp")
		.file("src/FastNoise2/src/FastSIMD/FastSIMD_Level_SSE41.cpp")
		.file("src/FastNoise2/src/FastSIMD/FastSIMD_Level_SSE42.cpp")
		.file("src/FastNoise2/src/FastSIMD/FastSIMD_Level_SSSE3.cpp")
		.file("src/FastNoise2/src/FastSIMD/FastSIMD.cpp")
		.cpp(true)
		.define("FASTNOISE_EXPORT", None);

	let target_os = std::env::var("CARGO_CFG_TARGET_OS");
	match target_os.as_ref().map(|x| &**x) {
		Ok("linux") => {
			build
				.flag("-std=c++17")
				.flag("-w")
				.flag("-march=native")
				.compile("fastnoise2");
		}
		Ok("windows") => {
			build.flag("/std:c++17").flag("/EHsc").compile("fastnoise2");
		}
		target_os => panic!("unknown target os {:?}!", target_os),
	}
}
