pub use libc;

use libc::{c_char, c_float, c_int, c_uint, c_void};

#[link(name = "fastnoise2")]
extern "C" {
	pub fn fnNewFromEncodedNodeTree(encodedString: *const c_char, simdLevel: c_uint)
		-> *mut c_void;
	pub fn fnDeleteNodeRef(node: *mut c_void);

	pub fn fnGetSIMDLevel(node: *const c_void) -> c_uint;
	pub fn fnGetMetadataID(node: *const c_void) -> c_int;

	pub fn fnGenUniformGrid2D(
		node: *const c_void,
		noiseOut: *mut c_float,
		xStart: c_int,
		yStart: c_int,
		xSize: c_int,
		ySize: c_int,
		frequency: c_float,
		seed: c_int,
		outputMinMax: *mut c_float,
	);

	pub fn fnGenUniformGrid3D(
		node: *const c_void,
		noiseOut: *mut c_float,
		xStart: c_int,
		yStart: c_int,
		zStart: c_int,
		xSize: c_int,
		ySize: c_int,
		zSize: c_int,
		frequency: c_float,
		seed: c_int,
		outputMinMax: *mut c_float,
	);

	pub fn fnGenUniformGrid4D(
		node: *const c_void,
		noiseOut: *mut c_float,
		xStart: c_int,
		yStart: c_int,
		zStart: c_int,
		wStart: c_int,
		xSize: c_int,
		ySize: c_int,
		zSize: c_int,
		wSize: c_int,
		frequency: c_float,
		seed: c_int,
		outputMinMax: *mut c_float,
	);

	pub fn fnGenPositionArray2D(
		node: *const c_void,
		noiseOut: *mut c_float,
		count: c_int,
		xPosArray: *const c_float,
		yPosArray: *const c_float,
		xOffset: c_float,
		yOffset: c_float,
		seed: c_int,
		outputMinMax: *mut c_float,
	);

	pub fn fnGenPositionArray3D(
		node: *const c_void,
		noiseOut: *mut c_float,
		count: c_int,
		xPosArray: *const c_float,
		yPosArray: *const c_float,
		zPosArray: *const c_float,
		xOffset: c_float,
		yOffset: c_float,
		zOffset: c_float,
		seed: c_int,
		outputMinMax: *mut c_float,
	);

	pub fn fnGenPositionArray4D(
		node: *const c_void,
		noiseOut: *mut c_float,
		count: c_int,
		xPosArray: *const c_float,
		yPosArray: *const c_float,
		zPosArray: *const c_float,
		wPosArray: *const c_float,
		xOffset: c_float,
		yOffset: c_float,
		zOffset: c_float,
		wOffset: c_float,
		seed: c_int,
		outputMinMax: *mut c_float,
	);

	pub fn fnGenTileable2D(
		node: *const c_void,
		noiseOut: *mut c_float,
		xSize: c_int,
		ySize: c_int,
		frequency: c_float,
		seed: c_int,
		outputMinMax: *mut c_float,
	);

	pub fn fnGenSingle2D(node: *const c_void, x: c_float, y: c_float, seed: c_int) -> c_float;
	pub fn fnGenSingle3D(
		node: *const c_void,
		x: c_float,
		y: c_float,
		z: c_float,
		seed: c_int,
	) -> c_float;
	pub fn fnGenSingle4D(
		node: *const c_void,
		x: c_float,
		y: c_float,
		z: c_float,
		w: c_float,
		seed: c_int,
	) -> c_float;

	pub fn fnGetMetadataCount() -> c_int;
	pub fn fnGetMetadataName(id: c_int) -> *const c_char; // valid IDs up to `fnGetMetadataCount() - 1`
	pub fn fnNewFromMetadata(
		id: c_int,
		simdLevel: c_uint, /*FastSIMD::eLevel 0 = Auto*/
	) -> *mut c_void;

	pub fn fnGetMetadataVariableCount(id: c_int) -> c_int;
	pub fn fnGetMetadataVariableName(id: c_int, variableIndex: c_int) -> *const c_char;
	pub fn fnGetMetadataVariableType(id: c_int, variableIndex: c_int) -> c_int;
	pub fn fnGetMetadataVariableDimensionIdx(id: c_int, variableIndex: c_int) -> c_int;
	pub fn fnGetMetadataEnumCount(id: c_int, variableIndex: c_int) -> c_int;
	pub fn fnGetMetadataEnumName(
		id: c_int,
		variableIndex: c_int,
		enumIndex: c_int,
	) -> *const c_char;
	pub fn fnSetVariableFloat(node: *mut c_void, variableIndex: c_int, value: c_float) -> bool;
	pub fn fnSetVariableIntEnum(node: *mut c_void, variableIndex: c_int, value: c_int) -> bool;

	pub fn fnGetMetadataNodeLookupCount(id: c_int) -> c_int;
	pub fn fnGetMetadataNodeLookupName(id: c_int, nodeLookupIndex: c_int) -> *const c_char;
	pub fn fnGetMetadataNodeLookupDimensionIdx(id: c_int, nodeLookupIndex: c_int) -> c_int;
	pub fn fnSetNodeLookup(
		node: *mut c_void,
		nodeLookupIndex: c_int,
		nodeLookup: *const c_void,
	) -> bool;

	pub fn fnGetMetadataHybridCount(id: c_int) -> c_int;
	pub fn fnGetMetadataHybridName(id: c_int, hybridIndex: c_int) -> *const c_char;
	pub fn fnGetMetadataHybridDimensionIdx(id: c_int, hybridIndex: c_int) -> c_int;
	pub fn fnSetHybridNodeLookup(
		node: *mut c_void,
		hybridIndex: c_int,
		nodeLookup: *const c_void,
	) -> bool;
	pub fn fnSetHybridFloat(node: *mut c_void, hybridIndex: c_int, value: c_float) -> bool;
}
